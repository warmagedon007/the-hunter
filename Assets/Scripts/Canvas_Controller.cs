﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_Controller : MonoBehaviour
{
    Enemy_Controller Parent;

    [SerializeField] Slider HealthSlider, StaminaSlider;
    private void Start()
    {
        Parent = GetComponentInParent<Enemy_Controller>();
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
        HealthSlider.maxValue = Parent.MaxHealth;
        HealthSlider.value = Parent.Health;
        StaminaSlider.maxValue = Parent.MaxStamina;
        StaminaSlider.value = Parent.Stamina;
    }
}
