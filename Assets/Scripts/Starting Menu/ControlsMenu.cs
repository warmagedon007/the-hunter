﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsMenu : MonoBehaviour
{
    public enum CtrlKey { Forward, Backward, Left, Right}
    #region Controls
    [SerializeField] GameObject prephab;

    static Dictionary<CtrlKey, KeyCode> controls;

    public static KeyCode GetKey(CtrlKey key) => controls[key];

    private void Start()
    {
        foreach (CtrlKey k in System.Enum.GetValues(typeof(CtrlKey)))
            controls[k] = PlayerPrefs.HasKey(k.ToString()) ? (KeyCode)PlayerPrefs.GetInt(k.ToString()) : KeyCode.None;

        int y = -100;
        foreach (int v in System.Enum.GetValues(typeof(CtrlKey)))
        {
            var instance = Instantiate(prephab, transform);
            instance.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,y);
            childrens.Add(instance.GetComponent<RectTransform>());
            var control = instance.GetComponent<preController>();
            control.Name = System.Enum.GetName(typeof(CtrlKey), v);
            control.Key = controls[(CtrlKey)v];
            y -= 100;
        }
        offset = childrens.Capacity > 5 ? 200 * (childrens.Capacity - 5) : 0;
    }

    public void Save()
    {
        for (int i = 0; i < childrens.Capacity; i++)
        {
            controls[(CtrlKey)i] = childrens[i].GetComponent<preController>().Key;
        }
        foreach (CtrlKey k in controls.Keys) PlayerPrefs.SetInt(k.ToString(), (int)controls[k]);
    }

    #endregion
    Scrollbar slider;
    int offset;
    List<RectTransform> childrens;

    private void Awake()
    {
        controls = new Dictionary<CtrlKey, KeyCode>();
        slider = GetComponentInChildren<Scrollbar>();
        childrens = new List<RectTransform>();
    }



    public void SliderControl()
    {
        int y = 0;
        foreach (var pre in childrens)
        {
            y -= 100;
            pre.anchoredPosition = new Vector3(0, y + offset * slider.value, 0);
        }
    }

}
