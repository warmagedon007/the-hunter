﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsMenu : MonoBehaviour
{
    public Dropdown resolutions;
    public Toggle fullscreen;
    public Slider brightness, qualityLevel, audioLevel;

    private void Awake()
    {
        Resolution cures = Screen.currentResolution;
        int index = 0, i = 0;
        List<string> ret = new List<string>();
        foreach (var resolution in Screen.resolutions)
        {
            index++;
            ret.Add(resolution.width + "X" + resolution.height + " " + resolution.refreshRate + "Hz");
            if (resolution.Equals(cures)) i = index;
        }
        resolutions.AddOptions(ret);
        qualityLevel.value = QualitySettings.GetQualityLevel();
        resolutions.value = i;
        fullscreen.isOn = Screen.fullScreen;
        brightness.value = RenderSettings.ambientLight.grayscale;
        audioLevel.value = AudioListener.volume;
    }

    public void ValueChanges()
    {
        RenderSettings.ambientLight = new Color(brightness.value, brightness.value, brightness.value);
        Screen.SetResolution(Screen.resolutions[resolutions.value].width, Screen.resolutions[resolutions.value].height, fullscreen.isOn, Screen.resolutions[resolutions.value].refreshRate);
        QualitySettings.SetQualityLevel(int.Parse(qualityLevel.value.ToString()));
        AudioListener.volume = audioLevel.value;
    }
}
