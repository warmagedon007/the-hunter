﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    #region transitions

    public CanvasGroup ActivePanel;
    public float transitionTime;

    public void Transition(CanvasGroup Into)
    {
        StartCoroutine(transition(Into));
    }

    IEnumerator transition(CanvasGroup Into)
    {
        ActivePanel.blocksRaycasts = false; ActivePanel.interactable = false;
        for (float time = 0; time < transitionTime; time += Time.fixedDeltaTime)
        {
            ActivePanel.alpha -= (Time.fixedDeltaTime / transitionTime);
            Into.alpha += (Time.fixedDeltaTime / transitionTime);
            yield return new WaitForFixedUpdate();
        }
        Into.blocksRaycasts = true; Into.interactable = true;
        ActivePanel = Into;
    }

    #endregion

    public void Exit()
    {
        Application.Quit();
    }
}
