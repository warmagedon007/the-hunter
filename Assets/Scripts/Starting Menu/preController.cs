﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class preController : MonoBehaviour
{
    [SerializeField] new Text name, k;

    public string Name { get => name.text; set => name.text = value; }

    public KeyCode Key;

    void UpdateKey()
    {
        bool flag = true;
        foreach (KeyCode kcode in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (k.text.ToUpper().CompareTo(kcode.ToString()) == 0)
            {
                flag = false;
                Key = kcode;
                break;
            }
        }

        Debug.Log(flag);
        k.color = flag ? Color.yellow : Color.red;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) UpdateKey();
    }
}
