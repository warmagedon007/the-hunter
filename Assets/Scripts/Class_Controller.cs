﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;

public abstract class Class_Controller : MonoBehaviour, ISerializationCallbackReceiver
{


    [SerializeField] protected bool debug;


    #region attributes and derivative

    [Header("Ability Scores")]
    [SerializeField] int Strength;
    [SerializeField] int Dexterity, Intelligence, Faith;

    public int MaxHealth { get => 10 + Strength*5; }

    public int MaxStamina { get => 200 + Dexterity * 100; }

    public int MaxMana { get => 5 + Intelligence * 10; }

    public int MaxFavors { get => Faith * 5; }

    [Header("Stats")]
    int health;
    int stamina, mana, favors;

    #endregion

    #region Health System

    public enum DamageType { Slashing, Blunt }

    [Serializable]
    public class serialized_Dict<v> : Dictionary<DamageType, v>, ISerializationCallbackReceiver
    {
        [SerializeField]
        List<v> values = new List<v>(Enum.GetValues(typeof(DamageType)).Length);

        public void OnAfterDeserialize()
        {
            Clear();
            foreach (int i in Enum.GetValues(typeof(DamageType)))

                Add((DamageType)i, values[i]);
        }

        public void OnBeforeSerialize()
        {
            values.Clear();
            foreach (DamageType i in Enum.GetValues(typeof(DamageType)))
                values.Add(this[i]);
        }
    }

    [Serializable]
    class resistance : serialized_Dict<float> { }

    [Serializable]
    class armor : serialized_Dict<int> { }

    [Header("Armor")]
    [SerializeField] [HideInInspector] resistance resistences;

    [SerializeField] [HideInInspector] armor armors;

    public int Health { get => health; }

    void Die()
    {
        animator.enabled = false;
    }

    public void TakeDamage(Dictionary<DamageType, int> damages)
    {
        foreach (DamageType damage in damages.Keys)
        {
            int d = (int)(damages[damage] * (1 - resistences[damage])) - armors[damage];
            health -= d < 0 ? 0 : d;
        }
        if (health <= 0) Die();
    }

    public void Heal(int value)
    {
        health += value;
        if (health > MaxHealth) health = MaxHealth;
    }

    #endregion

    #region Stamina System

    int staminaRegen { get => Dexterity + 1; }

    public int Stamina { get => stamina; }

    private void FixedUpdate()
    {
        if (stamina < MaxStamina) stamina += (int)(staminaRegen * Time.fixedDeltaTime);
        UIManager();
    }

    public bool DrainStamina(int s)
    {
        if (stamina < s) return false;
        stamina -= s; return true;
    }

    #endregion

    #region Animation

    float maxSpeed { get => Dexterity * 2 + Strength; }

    protected abstract Vector3 MovementDirection();

    protected Animator animator;

    const float walkSpeed = 1;

    protected void OnAnimatorMove()
    {
        
        transform.Translate(MovementDirection().normalized * walkSpeed * Time.deltaTime);
        animator?.SetFloat("Velocity", MovementDirection().magnitude);
        
    }

    #endregion

    #region UI
    
    List<int> armorStats; List<float> resistanceStats;

    public void OnBeforeSerialize()
    {
        armorStats.Clear(); resistanceStats.Clear();
        foreach (DamageType i in Enum.GetValues(typeof(DamageType)))
        {
            armorStats.Add(armors[i]); resistanceStats.Add(resistences[i]);
        }
    }

    public void OnAfterDeserialize()
    {
        armors.Clear(); resistences.Clear();
        foreach (int i in Enum.GetValues(typeof(DamageType)))
        {
            try { armors.Add((DamageType)i, armorStats[i]); resistences.Add((DamageType)i, resistanceStats[i]); }
            catch { armors.Add((DamageType)i, 0); resistences.Add((DamageType)i, 0f); }
        }
    }

    #endregion

    public Class_Controller()
    {
        armors = new armor(); resistences = new resistance();
        armorStats = new List<int>(); resistanceStats = new List<float>();
        foreach (DamageType item in Enum.GetValues(typeof(DamageType)))
        {
            armors.Add(item, 0); resistences.Add(item, 0f);
        }
    }

    protected abstract void UIManager();

    protected void Awake()
    {
        health = MaxHealth;
        animator = GetComponent<Animator>();
        if (transform.GetComponentsInChildren<Rigidbody>().Length == 0)
            this.gameObject.AddComponent(typeof(Rigidbody));
        gameObject.layer = 8;
    }

}

[CustomEditor(typeof(Class_Controller))]
public class Visualizer : Editor
{
    SerializedProperty armor, resistance;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        armor = serializedObject.FindProperty("armors").FindPropertyRelative("values");
        resistance = serializedObject.FindProperty("resistences").FindPropertyRelative("values");

        DrawDefaultInspector();

        EditorGUILayout.BeginFoldoutHeaderGroup(true, "Armor");

        foreach(int i in Enum.GetValues(typeof(Class_Controller.DamageType))) 
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(Enum.GetName(typeof(Class_Controller.DamageType), i));
            armor.GetArrayElementAtIndex(i).intValue = EditorGUILayout.IntField(armor.GetArrayElementAtIndex(i).intValue);
            EditorGUILayout.EndHorizontal();
            resistance.GetArrayElementAtIndex(i).floatValue = EditorGUILayout.Slider(resistance.GetArrayElementAtIndex(i).floatValue, 0, 1);

        }

        EditorGUILayout.EndFoldoutHeaderGroup();

        serializedObject.ApplyModifiedProperties();
    }

    
}