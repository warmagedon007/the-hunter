﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEditor;

public class Player_controller : Class_Controller
{

    protected override Vector3 MovementDirection() => new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

    Transform view { get => animator.GetBoneTransform(HumanBodyBones.Head); }

    Vector3 headRotation;

    void MouseControl()
    {
        view.eulerAngles = headRotation;
        transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X")));
        view?.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y"), 0));
        headRotation = view.eulerAngles;
    }

    private void LateUpdate()
    {
        MouseControl();
    }

    private new void Awake()
    {
        base.Awake();
        Camera eyes = view.GetChild(0).gameObject.AddComponent(typeof(Camera)) as Camera;
        eyes.tag = "MainCamera";
        eyes.nearClipPlane = 0.1f;
        headRotation = view.eulerAngles;
        gameObject.tag = "Player";
    }

    protected override void UIManager()
    {
        
    }
}

[CustomEditor(typeof(Player_controller))]
public class Player_Visualizer : Visualizer
{

}