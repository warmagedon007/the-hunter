﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class Enemy_Controller : Class_Controller
{

    [SerializeField] GameObject canvas;
    

    protected override Vector3 MovementDirection()
    {
        try
        {
            transform.LookAt(targets[0].position, Vector3.up);
            var v = transform.InverseTransformDirection(targets[0].position - transform.position);
            v.y = 0;
            return v;
        }
        catch
        {
            return new Vector3();
        }
    }

    protected override void UIManager()
    {
        canvas?.transform.LookAt(Camera.main.transform);
    }

    #region FOV
    [Space(10)] [Header("Field of view")]
    [SerializeField] [Range(0, 30)] float RadiusView; public float Radius { get => RadiusView; }
    [SerializeField] [Range(0, 80)] float angle; public float Angle { get => angle; }
    [SerializeField] string[] notEnemies;
    public Transform[] targets;

    Transform head { get => animator.GetBoneTransform(HumanBodyBones.Head).GetChild(0); }
    Transform[] getTargets()
    {
        List<Transform> trgts = new List<Transform>();
        foreach(Collider Ptargets in Physics.OverlapSphere(head.position,RadiusView,1 << 8))
        {
            if (Ptargets.transform.position == transform.position) continue;
            if(checkPosition(Ptargets)) 
                trgts.Add(Ptargets.transform);
        }
        return trgts.ToArray();
    }

    bool checkPosition(Collider collider)
    {
        foreach (string item in notEnemies)
            if (collider.CompareTag(item))
            {
                if (debug) Debug.Log($"{collider.name} didnt pass first test");
                return false;
            }  
        var direction = collider.transform.position - head.position;
        if (Vector3.Angle(head.forward, direction) > angle)
        {
            if (debug) Debug.Log($"{collider.name} didnt pass second test");
            return false;
        }
        if (!Physics.Raycast(head.position, direction, 1 << 8))
        {
            if (debug) Debug.Log($"{collider.name} didnt pass third test");
            return false;
        }
        return true;
    }


    IEnumerator field_of_view()
    {
        while(true)
        {
            yield return new WaitForEndOfFrame();
            targets = getTargets();
        }
    }
    #endregion

    private new void Awake()
    {
        base.Awake();
        Instantiate(canvas).transform.SetParent(transform);
    }

    private void Start()
    {
        StartCoroutine(field_of_view());
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}

[CustomEditor(typeof(Enemy_Controller))]
public class FOV_visualized:Editor
{
    private void OnSceneGUI()
    {
        Enemy_Controller fow = (Enemy_Controller)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fow.transform.position, Vector3.up, Vector3.forward, 360, fow.Radius);
        Vector3 viewAngleA = fow.DirFromAngle(-fow.Angle, false);
        Vector3 viewAngleB = fow.DirFromAngle(fow.Angle, false);

        Handles.color = Color.red;
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleA * fow.Radius);
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleB * fow.Radius);

        Handles.color = Color.green;
        try
        {
            Handles.DrawLine(fow.transform.position, fow.targets[0].position);
        }
        catch
        {

        }
    }

}
