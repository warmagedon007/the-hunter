﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public class Damage_Contoller : MonoBehaviour
{
    public class Damage : Class_Controller.serialized_Dict<int> { }

    [SerializeField] Damage damage;
    public int this[Class_Controller.DamageType type] => damage[type];
}